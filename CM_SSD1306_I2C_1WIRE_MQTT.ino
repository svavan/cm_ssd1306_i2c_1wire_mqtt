// WiFi includes
#include <OneWire.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// OTA Includes
//#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>

OneWire ds  (10);

const char *ssid         = "IoT";
const char *password     = "geektimes_ru";

//--------------------------------------------------------------------------
const char *mqtt_server = "192.168.1.251"; // Имя сервера MQTT
const int mqtt_port = 1883; // Порт для подключения к серверу MQTT
const char *mqtt_user = "Login"; // Логи от сервер
const char *mqtt_pass = "Pass"; // Пароль от сервера

WiFiClient wclient;
PubSubClient client(wclient);
//--------------------------------------------------------------------------


#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`

// Initialize the OLED display using brzo_i2c
// D3 -> SDA
// D5 -> SCL
// SSD1306Brzo display(0x3c, D3, D5);

// Initialize the OLED display using Wire library
SSD1306  display(0x3c, 4, 5);

//==============================================================================
float powerVoltage;
int zeroPoint, ADCval, current, milliAmpPerUnit;
unsigned long timeStart, timeWork, tRelayStateChange;
unsigned long timeoutMain = 200;
unsigned long timeoutRelay = 1000;

unsigned long mqttTimeout = 30000;
unsigned long mqttSendTimeout = 2000;
unsigned long mqttLastSendTime = 0;
unsigned long mqttLastConectTime;

unsigned long ssdShiftTimeout = 10000;
unsigned long ssdLastShiftTime = 0;
int ssdLastShiftPos = 0;
int dx, dy;

unsigned long networkReconectTimeout = 30000;
unsigned long networkLastReconect = 0;

int relay_pin = 13;
int keyUp = 14;
int keyDown = 12;
bool networkIsReady = false;


//---- EEPROM ----------------------------------------------------------------

int maxCurrent = 1000;
//bool maxCurrentSend = false;
bool relayIsOn = true;
char tempPrgUpLevel[3];// = {50,50,50};


//-----------------------------------------------------------------------------

bool blockRelay = false;
byte data[12];

//-----------------------------------------------------------------------------
// = {0x28, 0xB3, 0x6C, 0x05, 0x05, 0x00, 0x00, 0xC4};// сдвуноженный two leg
// = {0x28, 0x5E, 0xD6, 0x34, 0x01, 0x00, 0x00, 0x0C}; //от аквариума aquarium
// = {0x28, 0x2B, 0xE4, 0x04, 0x05, 0x00, 0x00, 0xAE}; // синий blue
//-----------------------------------------------------------------------------
byte addr_ds[3][8] = {{0x28, 0xB3, 0x6C, 0x05, 0x05, 0x00, 0x00, 0xC4},
  {0x28, 0x5E, 0xD6, 0x34, 0x01, 0x00, 0x00, 0x0C},
  {0x28, 0x2B, 0xE4, 0x04, 0x05, 0x00, 0x00, 0xAE}
} ;// сдвуноженный two leg

float temp[3];
byte lastSensor = 2;
bool conversionStartFlag[3] = {false, false, false};
unsigned long conversionStartTime[3];

//-----------------------------------------------------------------------------

//==============================================================================
void saveParam()
{
  //int maxCurrent = 1000;
  //byte tempPrgUpLevel[3];

  EEPROM.begin(8);

  byte b1, b2;

  //b1 = (maxCurrent & 0xff00) >> 8;
  //b2 = maxCurrent & 0x00ff;

  b1 = highByte(maxCurrent);
  b2 = lowByte(maxCurrent);

  if (EEPROM.read(0) != b1)
    EEPROM.write(0, b1);

  if (EEPROM.read(1) != b1)
    EEPROM.write(1, b2);

  if (EEPROM.read(2) != tempPrgUpLevel[0])
    EEPROM.write(2, tempPrgUpLevel[0]);

  if (EEPROM.read(3) != tempPrgUpLevel[1])
    EEPROM.write(3, tempPrgUpLevel[1]);

  if (EEPROM.read(4) != tempPrgUpLevel[2])
    EEPROM.write(4, tempPrgUpLevel[2]);

  EEPROM.commit();

}
//-----------------------------------------------------------------------------
void loadParam()
{
  EEPROM.begin(8);
  maxCurrent =  (EEPROM.read(0) << 8) + EEPROM.read(1);
  tempPrgUpLevel[0] = EEPROM.read(2);
  tempPrgUpLevel[1] = EEPROM.read(3);
  tempPrgUpLevel[2] = EEPROM.read(4);
}
//==============================================================================
bool networkReady() {
  WiFi.mode(WIFI_STA);
  WiFi.begin ( ssid, password );

  display.clear();
  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
  display.drawString(DISPLAY_WIDTH / 2, 8, "WiFi connecting...");
  display.display();

  int count = 0;

  while ( (WiFi.status() != WL_CONNECTED) || (WiFi.localIP().toString() == "0.0.0.0") )
  {

    delay ( 10 );
    count ++;
    if (count == 1000)
      return false;
  }

  //delay ( 100 );

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  mqttLastConectTime = millis();
  reconnect();

  ArduinoOTA.begin();

  ArduinoOTA.onStart([]() {
    display.clear();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
    display.drawString(DISPLAY_WIDTH / 2, 8, "Update");
    display.display();
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    display.drawProgressBar(4, 4, 120, 8, progress / (total / 100) );
    display.display();
  });

  ArduinoOTA.onEnd([]() {
    display.clear();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
    display.drawString(DISPLAY_WIDTH / 2, 8, "Restart");
    display.display();
  });

  return true;

}
//==============================================================================


//==============================================================================

void setup() {

  //EEPROM.begin(64);

  Serial.begin(115200);
  Serial.println("Start.");

  loadParam();

  pinMode(keyUp, INPUT);
  pinMode(keyDown, INPUT);

  pinMode(relay_pin, OUTPUT);

  relayProcessing();

  tRelayStateChange = millis();
  networkLastReconect = millis();

  Wire.begin();

  //WiFi.begin ( ssid, password );

  display.init();
  display.flipScreenVertically();
  display.setContrast(255);

  //display.clear();
  //display.setFont(ArialMT_Plain_10);
  //display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
  //display.drawString(DISPLAY_WIDTH / 2, 8, "WiFi connecting...");
  //display.display();

  networkIsReady = networkReady();
  // Wait for connection
  /*while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 10 );
    }*/


  /*client.setServer(mqtt_server, 1883);
    client.setCallback(callback);
    mqttLastConectTime = millis();
    reconnect();

    ArduinoOTA.begin();

    ArduinoOTA.onStart([]() {
    display.clear();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
    display.drawString(DISPLAY_WIDTH / 2, 8, "Update");
    display.display();
    });

    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    display.drawProgressBar(4, 4, 120, 8, progress / (total / 100) );
    display.display();
    });

    ArduinoOTA.onEnd([]() {
    display.clear();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
    display.drawString(DISPLAY_WIDTH / 2, 8, "Restart");
    display.display();
    });

  */

  timeStart = millis();
}

//==============================================================================

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  char buf[64];
  int i;
  for (i = 0; i < length; i++) {
    buf[i] = (char)payload[i];
    Serial.print((char)payload[i]);
  }
  buf[i] = 0x00;
  Serial.println();


  if (String(topic) == "SmartPowerSocket1/CurrentMax")
  {
    //Serial.println("For convert");
    //Serial.println(String(buf));
    maxCurrent = String(buf).toInt();
  }

  if (String(topic) == "SmartPowerSocker1/RelayEnable")
  {
    //Serial.println("For convert");
    //Serial.println(String(buf));
    int relayEnable = String(buf).toInt();
    if (relayEnable == 0)
      blockRelay = true;
    else
      blockRelay = false;

  }

  if (String(topic) == "SmartPowerSocker1/temp0PrgUpLevel")
  {
    int val = String(buf).toInt();
    if ((val > -127) && (val < 127))
      tempPrgUpLevel[0] = (char)val;
  }

  if (String(topic) == "SmartPowerSocker1/temp1PrgUpLevel")
  {
    int val = String(buf).toInt();
    if ((val > -127) && (val < 127))
      tempPrgUpLevel[1] = (char)val;
  }

  if (String(topic) == "SmartPowerSocker1/temp2PrgUpLevel")
  {
    int val = String(buf).toInt();
    if ((val > -127) && (val < 127))
      tempPrgUpLevel[2] = (char)val;
  }

  saveParam();

}
//------------------------------------------------------------------------------
void reconnect() {

  client.connect("ESP8266Client");
  client.subscribe("SmartPowerSocket1/CurrentMax");
  client.subscribe("SmartPowerSocker1/RelayEnable");
  client.subscribe("SmartPowerSocker1/temp0PrgUpLevel");
  client.subscribe("SmartPowerSocker1/temp1PrgUpLevel");
  client.subscribe("SmartPowerSocker1/temp2PrgUpLevel");

}
//------------------------------------------------------------------------------

void MqttSendSensorsVal()
{
  if (!networkIsReady)return;
  char msg[10];
  sprintf (msg, "%d", current);
  client.publish("SmartPowerSocket1/Current", msg);

  char str_temp[6];

  /* 4 is mininum width, 2 is precision; float value is copied onto str_temp*/
  if (temp[0] == -300)
    client.publish("SmartPowerSocker1/TempInside", "-");
  else
  {
    dtostrf(temp[0], 3, 1, str_temp);
    sprintf(msg, "%s", str_temp);
    client.publish("SmartPowerSocker1/TempInside", msg);
  }

  if (temp[1] == -300)
    client.publish("SmartPowerSocker1/Temp1", "-");
  else
  {
    dtostrf(temp[1], 3, 1, str_temp);
    sprintf(msg, "%s", str_temp);
    client.publish("SmartPowerSocker1/Temp1", msg);
  }

  if (temp[2] == -300)
    client.publish("SmartPowerSocker1/Temp2", "-");
  else
  {
    dtostrf(temp[2], 3, 1, str_temp);
    sprintf(msg, "%s", str_temp);
    client.publish("SmartPowerSocker1/Temp2", msg);
  }

  //sprintf (msg, "%d", relayIsOn);
  if (relayIsOn)
    client.publish("SmartPowerSocket1/relayIsOn", "ON");
  else
    client.publish("SmartPowerSocket1/relayIsOn", "OFF");

}

//------------------------------------------------------------------------------
void MqttSendMaxCurrent()
{
  if (!networkIsReady)return;
  char msg[6];
  sprintf (msg, "%d", maxCurrent);
  client.publish("SmartPowerSocket1/CurrentMax", msg, true);
}

void MqttSendBlockRelay()
{
  if (!networkIsReady)return;
  char msg[6];
  if (blockRelay == false)
    sprintf (msg, "%d", 1);
  else
    sprintf (msg, "%d", 0);
  client.publish("SmartPowerSocker1/RelayEnable", msg, true);
}


//==============================================================================
//==============================================================================
//==============================================================================
void loop() {



  if (networkIsReady)
    ArduinoOTA.handle();

  //----------------------------------------------------------
  if ((millis() - timeStart) > timeoutMain)
  {
    timeStart = millis();
    uint16_t I = getCurrentI2C();

    if (I != 0xFFFF)
      current = I;

    getAllTemp();
    keyProcessing();
    relayProcessing();


    if (networkIsReady)
      if (!client.connected())
      {
        if ((millis() - mqttLastConectTime) > mqttTimeout)
        {
          mqttLastConectTime = millis();
          reconnect();
        }
      }
      else
      {
        client.loop();
        if ((millis() - mqttLastSendTime) > mqttSendTimeout)
        {
          mqttLastSendTime = millis();
          MqttSendSensorsVal();
        }
      }

    draw_all();
    timeWork = millis() - timeStart;
    Serial.print(timeWork);
    Serial.print("  ");
    Serial.println(current);
    //Serial.println(WiFi.localIP().toString());

  }

  //----------------------------------------------------------
  if ((millis() - networkLastReconect) > networkReconectTimeout)
  {
    networkLastReconect = millis();
    if ( WiFi.status() != WL_CONNECTED )
    {
      Serial.print("Reconnect: ");
      Serial.println(networkLastReconect);
      networkIsReady = networkReady();
    }
  }
  //----------------------------------------------------------
  //keyProcessing();


}
//==============================================================================
//==============================================================================
//==============================================================================

void draw_all() {

  //int dx, dy;

  if ((millis() - ssdLastShiftTime) > ssdShiftTimeout)
  {
    ssdLastShiftTime = millis();

    if (ssdLastShiftPos == 0) {
      dx = -2;
      dy = 2;
    }

    if (ssdLastShiftPos == 1) {
      dx = 2;
      dy = 2;
    }

    if (ssdLastShiftPos == 2) {
      dx = 2;
      dy = -2;
    }

    if (ssdLastShiftPos == 3) {
      dx = -2;
      dy = -2;
    }

    if (ssdLastShiftPos == 4) {
      dx = 0;
      dy = 0;
    }

    ssdLastShiftPos++;
    if (ssdLastShiftPos == 5) ssdLastShiftPos = 0;
  }

  display.clear();

  display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
  display.setFont(ArialMT_Plain_10);


  if (relayIsOn)
    display.drawString(DISPLAY_WIDTH / 2 + dx, dy + 8, "R [ON] | " + WiFi.localIP().toString());
  else
    display.drawString(DISPLAY_WIDTH / 2 + dx, dy + 8, "R [OFF] | " + WiFi.localIP().toString());

  display.drawString(dx + DISPLAY_WIDTH / 2, dy + 22, "[ " + String((float)current / 1000, 2) + " / " + String((float)maxCurrent / 1000, 2) + " A] [ " + String(timeWork) + " mS]");


  int val = tempPrgUpLevel[0];
  if (val > 0x80)
    val = (128 - (val & 0x007F)) * -1;

  if (temp[0] == -300)
    display.drawString( dx + DISPLAY_WIDTH / 6, dy + 34, "-/" + String(val) );
  else
    display.drawString( dx + DISPLAY_WIDTH / 6, dy + 34, String(temp[0], 1) + "/" + String(val));


  val = tempPrgUpLevel[1];
  if (val > 0x80)
    val = (128 - (val & 0x007F)) * -1;


  if (temp[1] == -300)
    display.drawString(dx + DISPLAY_WIDTH / 2, dy + 34, "-/" + String(val) );
  else
    display.drawString(dx + DISPLAY_WIDTH / 2, dy + 34, String(temp[1], 1) + "/" + String(val));


  val = tempPrgUpLevel[2];
  if (val > 0x80)
    val = (128 - (val & 0x007F)) * -1;

  if (temp[2] == -300)
    display.drawString(dx +( DISPLAY_WIDTH / 6) * 5, dy + 34, "-/" + String(val) );
  else
    display.drawString(dx +( DISPLAY_WIDTH / 6) * 5, dy + 34, String(temp[2], 1) + "/" + String(val));

  display.display();
  //delay ( 50 );
}

//==============================================================================
int getAnalogVal(int countIteration) {

  int val = 0;

  for (int i = 0; i < countIteration; i++) {
    val = val + analogRead(A0);
  }
  return (val / countIteration);
}
//==============================================================================
uint16_t getAdc1Val(uint16_t timeout)
{
  uint32_t adc1AvgValue = 0;
  uint16_t countIteration = 0;
  unsigned long tStart = millis();
  uint16_t adc1 = 0;
  while ((millis() - tStart) < timeout)
  {
    adc1 = analogRead(A0);
    adc1AvgValue += adc1;
    countIteration++;
  }
  return ((uint16_t)(adc1AvgValue / countIteration));
}

//==============================================================================

uint16_t getAdc1NoiseAvgVal(uint16_t timeOfFix, uint16_t pause, uint16_t countIteration)
{
  uint32_t adcDeltaValue = 0;
  uint32_t adcDeltaValueMax = 0;
  //uint16_t countIteration = 0;

  for (uint16_t i = 0; i < countIteration; i++)
  {
    adcDeltaValue = abs(getAdc1Val(timeOfFix) - zeroPoint);
    if (adcDeltaValue > adcDeltaValueMax)
      adcDeltaValueMax = adcDeltaValue;
    delay(pause);
  }
  return ((uint16_t)(adcDeltaValueMax));
}
//==============================================================================
uint16_t getCurrentI2C() {

  unsigned long tStart = millis();
  unsigned long timeout = 10;

  uint16_t adc1 = 0;

  Wire.requestFrom(0x18, 3, true);    // request 6 bytes from slave device #8

  while ((millis() - tStart) < timeout)
    while (Wire.available())
    { // slave may send less than requested
      delay(1);
      byte c = Wire.read(); // receive a byte as character
      byte c1 = Wire.read(); // receive a byte as character
      byte crc = Wire.read();
      uint16_t val = c * 0x100 + c1;

      while (Wire.available())
        Wire.read();

      if (crc == c ^ c1)
      {
        return  (val);
      }
    }
  return (0xFFFF);
}
//==========================================================================================

float get_temp(byte sensorIndex) {

  //digitalWrite(led_pin, HIGH);

  if (!conversionStartFlag[sensorIndex])
  {
    conversionStartFlag[sensorIndex] = true;
    conversionStartTime[sensorIndex] = millis();
    ds.reset  ();
    ds.select  (addr_ds[sensorIndex]);   // Выбираем адрес
    ds.write  (0x44, 1); // Производим замер, в режиме паразитного питания
    return (-300);
  }
  else if ((millis() - conversionStartTime[sensorIndex]) > 750)
  {
    conversionStartFlag[sensorIndex] = false;

    float celsius;
    ds.reset  ();
    ds.select  (addr_ds[sensorIndex]);
    ds.write  (0xBE); // Считываем оперативную память датчика

    for  (int i = 0; i < 9; i++)
      data[i] = ds.read  ();

    //unsigned int raw =  (data[1] << 8) | data[0];
    unsigned int raw =  (data[1] << 8) | data[0];
    byte cfg =  (data[4] & 0x60);
    if  (cfg == 0x00) raw = raw << 3; // 9 bit resolution, 93.75 ms
    else if  (cfg == 0x20) raw = raw << 2; // 10 bit res, 187.5 ms
    else if  (cfg == 0x40) raw = raw << 1; // 11 bit res, 375 ms
    // default is 12 bit resolution, 750 ms conversion time
    //}
    //celsius =  (float)raw / 16.0;
    //digitalWrite(led_pin, LOW);

    Serial.print("[");
    Serial.println(raw, HEX);
    Serial.print("]");

    if (raw != 0xFFFF)
    {
      if (raw & 0x8000)
      {
        raw  = (0xFFFF - raw ) + 1;
        celsius =  (float)raw / 16.0 * -1;
        temp[sensorIndex] = celsius;
      }
      else
      {
        celsius =  (float)raw / 16.0;
        temp[sensorIndex] = celsius;
      }
    }
    else
      temp[sensorIndex] = -300;
    return celsius;
  }
};
//=================================================================================
void getAllTemp() {
  get_temp(lastSensor);

  if ((!conversionStartFlag[0]) && (!conversionStartFlag[1]) && (!conversionStartFlag[2]))
    lastSensor++;

  if (lastSensor == 3)
    lastSensor = 0;
}
//=================================================================================
void keyProcessing() {

  bool up;
  up = digitalRead(keyUp);

  bool down;
  down = digitalRead(keyDown);

  if (up && down)
  {
    blockRelay = true;
    MqttSendBlockRelay();
  }

  if (up && (maxCurrent < 10000))
    maxCurrent += 100;

  if (down && (maxCurrent > 50))
    maxCurrent -= 100;

  if (up || down)
  {
    MqttSendMaxCurrent();
    saveParam();
  }
}
//=================================================================================
void relayProcessing() {

  if (blockRelay)
  {
    relayIsOn = false;
    digitalWrite(relay_pin, relayIsOn);
  }

  if (!blockRelay)
  {

    if ((millis() - tRelayStateChange) > timeoutRelay)
    {

      tRelayStateChange = millis();

      if (current > maxCurrent)
        relayIsOn = false;
      else
        relayIsOn = true;

      int val = tempPrgUpLevel[0];
      if (val > 0x80)
        val = (128 - (val & 0x007F)) * -1;

      bool cerrentSensorSetpointExceeded = false;

      if (temp[0] > (float)val)
        cerrentSensorSetpointExceeded = true;
      if (temp[0] < (float)(val + 2))
        cerrentSensorSetpointExceeded = false;

      relayIsOn = relayIsOn & (!cerrentSensorSetpointExceeded);
      /*
        val = tempPrgUpLevel[1];
        if (val > 0x80)
        val = (128 - (val & 0x007F)) * -1;

        if (temp[1] > (float)val)
        cerrentSensorSetpointExceeded = true;
        if (temp[1] < (float)(val + 2))
        cerrentSensorSetpointExceeded = false;

        relayIsOn = relayIsOn & (!cerrentSensorSetpointExceeded);

        val = tempPrgUpLevel[2];
        if (val > 0x80)
        val = (128 - (val & 0x007F)) * -1;

        if (temp[2] > (float)val)
        cerrentSensorSetpointExceeded = true;
        if (temp[2] < (float)(val + 2))
        cerrentSensorSetpointExceeded = false;

        relayIsOn = relayIsOn & (!cerrentSensorSetpointExceeded);
      */

    }

    /*if (relayIsOn)
      digitalWrite(relay_pin, HIGH);
      else*/
    digitalWrite(relay_pin, relayIsOn);

  }
}


